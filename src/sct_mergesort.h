// SPDX-FileCopyrightText: 2023 2017 Deutsches Klimarechenzentrum GmbH (DKRZ)
//
// SPDX-License-Identifier: BSD-3-Clause

/**
 * @file mergesort.h
 * @brief merge sort declaration
 */


#ifndef MERGESORT_H
#define MERGESORT_H

#include "sct_config.h"

typedef struct strpos_struct {
  char str[SCT_LABEL_SIZE];
  int pos;
} strpos_type;


/** mergesort changing structured values
  *
  * @param[in,out]  v            data to be sorted
  * @param[in]      n            number of elements in v
 **/
void sct_mergesort_strpos(strpos_type *v, int n);

/** mergesort changing values and indices
  *
  * @param[in,out]  a            data to be sorted
  * @param[in]      n            length of data
  * @param[in,out]  idx          old index of sorted returned a
  * @param[in]      reset_index  override given idx by identity idx
  */
void sct_mergesort_index (char (*a)[SCT_LABEL_SIZE], int n, int *idx, int reset_index);

#endif
