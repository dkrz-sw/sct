// SPDX-FileCopyrightText: 2023 2017 Deutsches Klimarechenzentrum GmbH (DKRZ)
//
// SPDX-License-Identifier: BSD-3-Clause

#include "sct_mach.h"
#include "sct_collector.h"

int sct_init_f(const int tsize, const char* default_context_name,
               int default_comm_f) {
#ifdef HAVE_MPI
  MPI_Comm default_comm_c = MPI_Comm_f2c(default_comm_f);
#else
  int default_comm_c = default_comm_f;
#endif

  return sct_init(tsize, default_context_name, default_comm_c);
}

int sct_new_context_f(const char* name, const int comm_f) {
#ifdef HAVE_MPI
  MPI_Comm comm_c = MPI_Comm_f2c(comm_f);
#else
  int comm_c = comm_f;
#endif

  return sct_new_context(name, comm_c);
}
