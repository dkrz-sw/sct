// Copyright (C) 2017 Deutsches Klimarechenzentrum GmbH (DKRZ)
// SPDX-FileCopyrightText: 2023 2017 Deutsches Klimarechenzentrum GmbH (DKRZ)
//
// SPDX-License-Identifier: BSD-3-Clause

#ifndef _H_SCT_REPORTER
#define _H_SCT_REPORTER

#if HAVE_CONFIG_H
#  ifndef _H_CONFIG
#    define _H_CONFIG
#    include <config.h>
#  endif
#endif

//public+
#include <stdio.h>

/*! \brief print specific timer results
    \param[in] timer_choice SCT_SELECT_ALL or timer_id for output
    \param[in] proc_choice SCT_SELECT_ALL or SCT_REDUCE_ALL MPI-tasks for output
    \param[in] thread_choice SCT_SELECT_ALL or SCT_REDUCE_ALL OpenMP-threads for output
    \param[in] sp_merging merging operation for serial part of OpenMP timer
*/
void sct_single_report(int timer_choice, int proc_choice, int thread_choice, int sp_merging);

/*! \brief print all timer results
    \param[in] proc_choice SCT_SELECT_ALL or SCT_REDUCE_ALL MPI-tasks for output
    \param[in] thread_choice SCT_SELECT_ALL or SCT_REDUCE_ALL OpenMP-threads for output
    \param[in] sp_merging merging operation for serial part of OpenMP timer
*/
void sct_report(int proc_choice, int thread_choice, int sp_merging);

/*! \brief print all or specific  timer results to specified output stream
    \param[in] outstream_arg output stream to write results to
    \param[in] timer_choice SCT_SELECT_ALL or timer_id for output
    \param[in] proc_choice SCT_SELECT_ALL or SCT_REDUCE_ALL MPI-tasks for output
    \param[in] thread_choice SCT_SELECT_ALL or SCT_REDUCE_ALL OpenMP-threads for output
    \param[in] sp_merging merging operation for serial part of OpenMP timer
*/
void sct_stream_report(FILE *outstream_arg, int timer_choice, int proc_choice, int thread_choice, int sp_merging);

//public-

#endif
