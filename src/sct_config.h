// SPDX-FileCopyrightText: 2023 2017 Deutsches Klimarechenzentrum GmbH (DKRZ)
//
// SPDX-License-Identifier: BSD-3-Clause

/*
 * sct_config.h: configuration of sct package
 */

#ifndef _H_SCT_CONFIG
#define _H_SCT_CONFIG


//! number of timers if not set by user
//! can be set to any positive value
#define SCT_DEFAULT_TIMER_SIZE 300


//! maximum length of timer label
#define SCT_LABEL_SIZE 256


//! how deep can timer contexts go?
//! costs: 4 Bytes per context level
#define SCT_MAX_CONTEXT_DEPTH 15


//! how many nested timer levels are allowed without reallocation of datastructures
#define SCT_MAX_NEST_DEPTH 10


//! known real time methods
#define SCT_RTM_UNDEF                    0
#define SCT_RTM_READ_REAL_TIME           1
#define SCT_RTM_OMP_GET_WTIME            2
#define SCT_RTM_MPI_WTIME                4
#define SCT_RTM_CLOCK_GETTIME_MONOTONIC  8
#define SCT_RTM_CLOCK_GETTIME_REALTIME   16
#define SCT_RTM_GETTIMEOFDAY             32
#define SCT_RTM_RDTSCP                   64

#endif
